#!/usr/bin/env python3

import gitlab
import argparse
import requests
import json
import csv
import time
import os
from datetime import date

class Group_Exporter():

    gitlab_url = "https://gitlab.com"
    api_url = "https://gitlab.com/api/v4/"
    group = None
    token = None
    headers = None
    groups = []
    projects = []
    audit_events = []

    def __init__(self, args):
        if args.gitlab:
            self.gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
            self.api_url = self.gitlab_url + "api/v4/"
        self.token = os.environ[args.token_environment_variable]
        gl = gitlab.Gitlab(self.gitlab_url, private_token=self.token)
        self.group = gl.groups.get(args.group)
        self.headers = {'PRIVATE-TOKEN': self.token}
        # I may not need subgroups here as group export contains them already
        self.get_subgroups(gl, self.group)
        self.get_projects(gl, self.group)

    def get_subgroups(self, gl, group):
        print("Getting subgroups for group %s" % group.path)
        self.groups.append(group)
        subgroups = group.subgroups.list(as_list=False)
        for subgroup in subgroups:
            subgroup_object = gl.groups.get(subgroup.id)
            self.get_subgroups(gl, subgroup_object)

    def get_projects(self, gl, group):
        print("Getting projects for group %s" % group.path)
        group_projects = group.projects.list(include_subgroups=True, as_list=False)
        for project in group_projects:
            self.projects.append(gl.projects.get(project.id))

    def create_directories(self):
        paths = {}
        today = str(date.today())
        paths["group"] = self.group.attributes["full_path"] + "-" + today
        paths["projects"] = paths["group"] + "/projects/"
        try:
            os.makedirs(paths["projects"])
        except:
            pass
        return paths

    def export(self, groups, projects):
        paths = self.create_directories()
        print("Exporting %s and subgroups" % self.group.attributes["full_path"])
        export = self.group.exports.create()
        while True:
            time.sleep(5)
            try:
                with open(paths["group"] + '/groups.tar.gz', 'wb') as f:
                    export.download(streamed=True, action=f.write)
                    break
            except gitlab.exceptions.GitlabHttpError:
                print("Export not ready yet, waiting")

        for project in projects:
            print("Exporting %s" % project.attributes["path_with_namespace"])
            export = project.exports.create()
            # Wait for the 'finished' status
            export.refresh()
            while export.export_status != 'finished':
                time.sleep(5)
                export.refresh()

            # Download the result
            with open(paths["projects"] + str(project.id) + '_' + project.attributes["path"] + '.tar.gz' , 'wb') as f:
                export.download(streamed=True, action=f.write)

    def get_audit_events(self, resource, resource_type):
        path = "/%s/%s/audit_events" % (resource_type, resource.id)
        page = 1
        event_log = []
        while True:
            events = self.request_resource(path, page)
            if events:
                event_log.extend(events)
                if len(events) < 100:
                    break
                page += 1
            else:
                print("Can't get resource: %s page %s" % (path, page))
        return event_log

    def request_resource(self, path, page):
        default_params = { "page":page, "per_page":100}
        request_url = self.api_url + path
        data = requests.get(request_url, params=default_params, headers=self.headers)
        return data.json()

    def flatten(self, event_logs):
        flat_logs = []
        #fields = ["author", "object", "object_type","action_type","action","target_type","target_details","IP","date"]
        for event in event_logs:
            if not isinstance(event, dict):
                continue
            flat_event = {}
            flat_event["author"] = event["details"]["author_name"]
            flat_event["object"] = event["details"]["entity_path"]
            flat_event["object_type"] = event["entity_type"]
            if "add" in event["details"]:
                flat_event["action_type"] = "ADD"
                flat_event["action"] = event["details"]["add"]
                if "as" in event["details"]:
                    flat_event["action"] += " as " + event["details"]["as"]
            elif "remove" in event["details"]:
                flat_event["action_type"] = "REMOVE"
                flat_event["action"] = event["details"]["remove"]
            elif "change" in event["details"]:
                flat_event["action_type"] = "CHANGE"
                flat_event["action"] = "%s from %s to %s" % (event["details"]["change"],event["details"]["from"],event["details"]["to"])
            else:
                flat_event["action_type"] = "OTHER"
                if "custom_message" in event["details"]:
                    flat_event["action"] = event["details"]["custom_message"]
                else:
                    flat_event["action"] = ""
            flat_event["target_type"] = event["details"]["target_type"]
            flat_event["target_details"] = event["details"]["target_details"]
            flat_event["IP"] = event["details"]["ip_address"]
            flat_event["date"] = event["created_at"]
            flat_logs.append(flat_event)
        return flat_logs

    def write_csv(self, events, file):
        with open(file, "w") as outfile:
            fields = ["author", "object", "object_type","action_type","action","target_details","target_type","IP","date"]
            reportwriter = csv.writer(outfile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            reportwriter.writerow(fields)
            for event in events:
                row = []
                for field in fields:
                    if field in event:
                        row.append(event[field])
                    else:
                        row.append("")
                reportwriter.writerow(row)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Export a GitLab group including all its projects')
    parser.add_argument('token_environment_variable', help='API token able to read the requested group')
    parser.add_argument('group', help='ID of the group to export')
    parser.add_argument('--gitlab', help='GitLab URL, defaults to https://gitlab.com/')
    args = parser.parse_args()
    exporter = Group_Exporter(args)
    exporter.export(exporter.groups, exporter.projects)


    #with open("group_audit_log.json", "w") as logfile:
    #    json.dump(flat_log, logfile, indent=4, ensure_ascii=False)
