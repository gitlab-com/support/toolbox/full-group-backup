# Specification

## Mandatory Features

- SECURITY FIRST! (Handling the complete set of data in a group hierarchy is businessd critical)
  - Secure handling of access token
    - Storage
    - Handover to script
    - Handling inside script
  - Secure communication
    - No delivery by mail by Gitlab (if possible)
    - Export packages (tar.gz) should be encrypted in Gitlab before download (if possible) (https ssh encryption + Gitlab authentication enough or not?)

- Functionality
  - Recursive export of all group data in all details (statistics???)
  - Export of all projects referenced in the group data
  - Export of git repository, pipeline configuration, issues, milestones and wiki for all the projects

- Configuration in a Separate File
  - Format of the configuration Data: YAML
  - Content of the configuration Data
    - ID of the (top most) groupe to be exported
    - Local path of the exported data

- Execution Environment
  - gitlab-runner with docker
    - Token handling: ??? e.g. in Gitlab protected CI environment variables
  - Batch process on a local Server (Linux or Windows)
    - Token handling: ???

- Technology
  - CLI client
  - Python 3

## Nice to Have Features

- Functionality
  - Reimport into a local Gitlab instance
    - Goal: Ensure consistency of exported data